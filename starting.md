## Elements to new documentation
Some old docments wrote by me that could be interesting to a new documentation about the GIMP brushes.

* Where and when the **.gih brushes** are better of the **.gbr and .vbr brushes**.
  * [Parametric versus Raster Brush](https://medium.com/graphic-arts-free-software/parametric-versus-raster-brushes-e77d36ce480c)
  * [Parametric Brush Tutorial](https://www.gimp.org/tutorials/Parametric_Brush/)

* .Gih brushes | Elements for the Tutorials
  * [GIMP Gih Matrix in Short | An Idea](http://forksanddrills.blogspot.com/2014/02/gimp-gih-matrix-in-short-part-i.html)
  * [GIMP Gih Brushes Usage on Painting - L'ubomir Zabadal](http://forksanddrills.blogspot.com/2012/12/time-elapsed-videos-to-zabadal-brushes.html)
  * [Improving Color Mixing with Gih Brushes on GIMP - An approach](http://forksanddrills.blogspot.com/2016/10/mixing-color-on-gimp.html)
  * [GIMP Animated Brushes](https://docs.gimp.org/en/gimp-using-animated-brushes.html)

* Other Things
  * [Gimp Image Pipe Format](https://gitlab.gnome.org/GNOME/gimp/blob/master/devel-docs/gih.txt)

## Portability
Some references to discuss the portability of raster brushes to other apps, mainly to Krita referred to the .gih and .gbr brushes.

 * [.GIH Brushes (Krita documentation)](https://docs.krita.org/en/general_concepts/file_formats/file_gih.html?highlight=gih%20brushes)
 * [.GIH Brush Tips (Krita documentation)](https://docs.krita.org/en/tutorials/krita-brush-tips/animated_brushes.html?highlight=brush%20tips)
 * [Difference of GIH brushes between Krita and GIMP (Krita Forum Issue)](https://forum.kde.org/viewtopic.php?f=139&t=159697)

## Comparison
Comparison and kind of brushes to paint and other scopes in the different graphic arts areas.

 * Criteria to compare brushes static and dynamics (.gbr and .gih brushes, respectively).
 * Brushes to paint and for the decorative or patterns scopes.
 * Verify how are the behaviors of .gih brushes when used with the same parameter on dynamics (angular, random, pressure, tilt x,y, velocity).

## Improvement Ideas
Main ideas and concepts to amplify the usage in different graphic art areas and improve the usability/sharing/etc.

 * [New instances to Brushes | Proposal - #994 Gitlab Issue](https://gitlab.gnome.org/GNOME/gimp/issues/994)
 * Change the precision of the raster brushes.
   * Parametric Brushes 32f | [Non-parametric brushes are limited to 8-bit integer precision, #2372 Gitlab Issue](https://gitlab.gnome.org/GNOME/gimp/issues/2372)
 * Provide the gbr and .gih with an icon.
 * Provide .gih brush with independent alignment and size layers > to produce lightweights brushes. [#1156 Gitlab Issue](https://gitlab.gnome.org/GNOME/gimp/issues/1156)
 * Provide the internal sort keys based [#994 Gitlab Issue](https://gitlab.gnome.org/GNOME/gimp/issues/994)
 * Provide raster and vector brushes to embedded the tool preset and eventual link to a paint dynamics (broken link issue, verify).

## Related improvements [Perhaps]

 *  Improve the parameter Direction on the Paint Dynamics... the effect when we change direction is very ugly. [#639 Gitlab Issue](https://gitlab.gnome.org/GNOME/gimp/issues/639) and [#424 Gitlab Issue](https://gitlab.gnome.org/GNOME/gimp/issues/424)
 *  Improve the performance with Spacing Paint Dynamics. [#1863 Gitlab Issue](https://gitlab.gnome.org/GNOME/gimp/issues/1863)
 *  Improve the hardness effect on the stain: the current method is deforming the stain excessively. [#1081 Gitlab Issue](https://gitlab.gnome.org/GNOME/gimp/issues/1081)
 *  Provide real-time preview of the effects of the tool preset + dynamics variations by the user. [#115 Gitlab Issue](https://gitlab.gnome.org/GNOME/gimp/issues/115) and [Others and around issues - notes](https://gui.gimp.org/index.php?title=Others_and_around)
 *  Brush Preview Instances based on Tool Options settings and current Paint Dynamics.
 *  Added polygonal shapes on .vbr brush. [Draft document on gui.gimp.org](https://gui.gimp.org/index.php?title=Parametric-brushes)
 *  Plugin to collect assets linked in the tool preset (tool options settings, tool preset, brush and paint dynamics) > export to share.
 *  Share Assets: [Gimpusers thread discussion](http://www.gimpusers.com/forums/gimp-developer/15489-brushpack-file-format Brush Pack File Format).
